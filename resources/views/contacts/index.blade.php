@extends('layouts.app')

@section('page-css')
    <link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css"></link>
@endsection

@section('content')
    <div class="container">
        @include('includes.alerts')
        <a href="{{ route('contact.create') }}" class="btn btn-primary mb-3 btn-add">New Contact</a>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">Contact List</div>

                    <div class="card-body">
                        <div class="table-responsive" style="padding: 10px;">
                            <table id="contacts" class="display table table-striped table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
@endsection

@section('bottom-js')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#contacts').DataTable({
                serverSide: true,
                ajax: "{{ route('contact.index') }}",
                columns: [
                    { name: 'id' },
                    { name: 'name' },
                    { name: 'email' },
                    { name: 'address' },
                    { name: 'contact' },
                    { name: 'action', orderable: false, searchable:false }
                ]
            });


            $(document).on('click', '.delete-contact', function() {
                let idContact = $(this).attr('data-id');
                
                $.confirm({
                    title: 'Confirm Delete',
                    content: 'Do you really want to Delete this Contact? This action cannot be undone.',
                    buttons: {
                        confirm: {
                            text: 'Confirm',
                            btnClass: 'btn-success',
                            action: function () {
                                $.ajax({
                                    type: 'POST',
                                    dataType: 'json',
                                    url: '{{ route("contact.delete") }}',
                                    data: {
                                        id: idContact
                                    },
                                    async: false,
                                    success: function(data) {
                                        if(data.status == 'Success') {
                                            $('a[rel="'+idContact+'"]').parent().parent().remove();

                                            $.alert({
                                                title: data.status,
                                                content: data.message
                                            });
                                        } else {
                                            $.alert({
                                                title: data.status,
                                                content: data.message
                                            });
                                        }
                                        
                                    },
                                    error: function(data) {
                                        $.alert({
                                            title: "Error",
                                            content: "Unexpected error. Try again."
                                        });
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            btnClass: 'btn-danger',
                            action: function () {
                                
                            }    
                        },
                    }

                });
            });
        });
    </script>
@endsection