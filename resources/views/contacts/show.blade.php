@extends('layouts.app')

@section('content')
    <div class="container">
        @include('includes.alerts')
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Show <strong><i>{{ $contact->name }}</i></strong></div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputName">Name:</label>
                                <span class="form-control">{{ $contact->name }}</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail">Email:</label>
                                <span class="form-control">{{ $contact->email }}</span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputAddress">Address:</label>
                                <span class="form-control">{{ $contact->address }}</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputContact">Contact:</label>
                                <span class="form-control">{{ $contact->contact }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer" style="background: white">
                        <div class="mc-footer">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <a href="{{ route('contact.index') }}"class="btn btn-primary float-left"><i class="fas fa-undo-alt"></i> Back</a>
                                    <a href="{{ route('contact.edit', $contact->id) }}" class="btn btn-warning m-1">
                                        Edit <i class="far fa-edit"></i>
                                    </a>
                                    <a href="#" data-id="{{ $contact->id }}" class="delete-contact btn btn-danger m-1 float-right">
                                        Delete <i class="far fa-trash-alt"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('bottom-js')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $('.delete-contact').on('click', function() {
                let idContact = $(this).attr('data-id');
                
                $.confirm({
                    title: 'Confirm Delete',
                    content: 'Do you really want to Delete this Contact? This action cannot be undone.',
                    buttons: {
                        confirm: {
                            text: 'Confirm',
                            btnClass: 'btn-success',
                            action: function () {
                                $.ajax({
                                    type: 'POST',
                                    dataType: 'json',
                                    url: '{{ route("contact.delete") }}',
                                    data: {
                                        id: idContact
                                    },
                                    async: false,
                                    success: function(data) {
                                        if(data.status == 'Success') {
                                            window.location.href = "{{ route('contact.index') }}";

                                            $.alert({
                                                title: data.status,
                                                content: data.message
                                            });
                                        } else {
                                            $.alert({
                                                title: data.status,
                                                content: data.message
                                            });
                                        }
                                    },
                                    error: function(data) {
                                        $.alert({
                                            title: "Error",
                                            content: "Unexpected error. Try again."
                                        });
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            btnClass: 'btn-danger',
                            action: function () {
                                
                            }    
                        },
                    }

                });
            });
        });
    </script>
@endsection