@extends('layouts.app')

@section('content')
    <div class="container">
        @include('includes.alerts')
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @if(isset($contact))
                        <div class="card-header">Update <strong><i>{{ $contact->name }}</i></strong></div>
                            <form method="post" action="{{ route('contact.update', $contact->id) }}">
                    @else
                        <div class="card-header">New Contact</strong></div>
                        <form method="post" action="{{ route('contact.store') }}">
                    @endif
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputName">Name:</label>
                                    <input type="text" name="name" class="form-control" id="inputName" value="{{ $contact->name ?? old('name') }}" placeholder="Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail">Email:</label>
                                    <input type="email" name="email" class="form-control" id="inputEmail" value="{{ $contact->email ?? old('email') }}" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputAddress">Address:</label>
                                    <input type="text" name="address" class="form-control" id="inputAddress" value="{{ $contact->address ?? old('address') }}" placeholder="Address">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputContact">Contact:</label>
                                    <input type="text" name="contact" class="form-control" id="inputContact" value="{{ $contact->contact ?? old('contact') }}" placeholder="Contact">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer" style="background: white">
                            <div class="mc-footer">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <a href="{{ route('contact.index') }}"class="btn btn-danger"><i class="fas fa-undo-alt"></i> Back</a>
                                        @if(isset($contact))
                                            <button type="submit" class="btn btn-success m-1 float-right">
                                                Update <i class="far fa-save"></i>
                                            </button>
                                        @else
                                            <button type="submit" class="btn btn-success m-1 float-right">
                                                Save <i class="far fa-save"></i>
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
@endsection

@section('bottom-js')
    <script>
        $(document).ready(function($) {
            $('#inputContact').mask('00 000 00 00');
        });
    </script>
@endsection