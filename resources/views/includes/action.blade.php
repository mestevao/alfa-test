<a class="btn btn-primary" href="{{ route('contact.show', $contact->id) }}">
	<i class="far fa-eye"></i>
</a>
@if(Auth::check())
    <a class="btn btn-warning" href="{{ route('contact.edit', $contact->id) }}"><i class="far fa-edit"></i></a>
    <a class="btn btn-danger delete-contact" data-id="{{ $contact->id }}" rel="{{ $contact->id }}" href="javascript::"><i class="far fa-trash-alt"></i></a>
@endif