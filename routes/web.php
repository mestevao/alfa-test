<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('contact.index');
});

Auth::routes();
Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('contacts')->group(function() {
	Route::get('', [App\Http\Controllers\ContactController::class, 'index'])->name('contact.index');
	Route::get('create', [App\Http\Controllers\ContactController::class, 'create'])->name('contact.create');
	Route::post('store', [App\Http\Controllers\ContactController::class, 'store'])->name('contact.store');
	Route::get('show/{id}', [App\Http\Controllers\ContactController::class, 'show'])->name('contact.show');
	
	Route::middleware(['auth'])->group(function() {
		Route::get('edit/{id}', [App\Http\Controllers\ContactController::class, 'edit'])->name('contact.edit');
		Route::post('update/{id}', [App\Http\Controllers\ContactController::class, 'update'])->name('contact.update');
		Route::post('delete', [App\Http\Controllers\ContactController::class, 'destroy'])->name('contact.delete');
	});
});
