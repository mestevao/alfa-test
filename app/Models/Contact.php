<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'address',
        'email',
        'contact',
        'deleted_by'
    ];

    public static function laratablesCustomAction($contact)
    {
    	return view('includes.action', compact('contact'))->render();
    }
}
